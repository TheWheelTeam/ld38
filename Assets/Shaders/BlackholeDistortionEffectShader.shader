﻿ Shader "Custom/MyImageEffectShader"
{
	Properties
	{
		[HideInInspector]
		_MainTex ("Texture", 2D) = "white" {}

		[HideInInspector]
		_DistToBlackhole ("Distance To Blackhole", Float) = -1 // -1 will be ignored

		[HideInInspector]
		_MaxDistance ("Max Distance", Range(0, 10000)) = 0

		[HideInInspector]
		_EffectCenterOffset ("Effect Center Offset", Vector) = (0, 0, 0, 0)

		[HideInInspector]
		_EffectRange ("Effect Range", Range(0.01, 10)) = 0 // 5

		[HideInInspector]
		_EffectStrength ("Effect Strength", Range(-1, 1)) = 0 // -0.42

		[HideInInspector]
		_ZoomLevel ("Zoom Level", Range(0.2, 8)) = 1 //

		[HideInInspector]
		_Dividers ("Dividers", Vector) = (1024, 1024, 1024, 1024)
		// (frequency.x, frequency.y, magnitude.x, magnitude.y)

		[HideInInspector]
		_WaveRange ("Wave Range", Range(0, 10000)) = 0

		[HideInInspector]
		_WaveFrequency ("Wave Frequency", Range(0, 128)) = 0
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			float _DistToBlackhole;
			float _MaxDistance;
			float4 _EffectCenterOffset;
			float _EffectRange;
			float _EffectStrength;
			float4 _Dividers; // (frequency.x, frequency.y, magnitude.x, magnitude.y)
			float _WaveRange; // distance from black hole to activate the waves
			float _WaveFrequency;

			fixed2 zoom(fixed level, fixed2 v)
			{
				return (v*level) + (1 - level) / 2;
			}

			fixed2 distort(fixed2 v)
			{
				if (_DistToBlackhole < 0) return v;

				fixed2 center = -v.xy + fixed2(0.5, 0.5) + _EffectCenterOffset;
				fixed dist = distance(v.xy, center);

				fixed multiplier = (_MaxDistance - _DistToBlackhole) / _MaxDistance; // [0..1]

				if (dist < _EffectRange)
				{
					v -= multiplier * _EffectStrength * normalize(center) * log(length(center));
					v = zoom(saturate((1 - multiplier) + 0.8), v);
				}

				return v;
			}

			fixed2 wave(fixed2 v)
			{
				if (_DistToBlackhole < 0) return v;
				if (_DistToBlackhole > _WaveRange) return v;
				
				// 200 is a magic number;
				// the minimum distance between the black hole and the player
				fixed multiplier = (_WaveRange - _DistToBlackhole) / (_WaveRange - 200);

				return v + multiplier * sin(_Time[3] * _WaveFrequency) * fixed2(
					sin(v.y/_Dividers.x * _Time[2]) / _Dividers.z, 
					sin(v.x/_Dividers.y * _Time[2]) / _Dividers.w);
			}

			fixed4 frag (v2f i) : SV_Target
			{
				return tex2D(_MainTex, wave(distort(i.uv)));
			}
			ENDCG
		}
	}
}
