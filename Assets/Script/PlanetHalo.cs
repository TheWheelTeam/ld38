using UnityEngine;
using UnityEngine.SceneManagement;

public class PlanetHalo : MonoBehaviour
{
    [Range(0, 3)]
    public float Size;

    public Color Color = Color.white;
    public bool ColorFromMaterial;

    private Light _light;

    private void Start()
    {
        _light = GetComponent<Light>();

        if (ColorFromMaterial)
        {
            var matName = transform.parent.GetComponent<Renderer>().material.name;
            _light.color = PlanetMaterialColor.ColorFromMaterial(matName);
        }
        else
        {
            _light.color = Color;
        }

        _light.range = transform.parent.parent.localScale.x * Size;
    }

    //    private void Start()
    //    {
    //        _light.color = Color;
    ////        _light.range = (UseDirectParent ? transform.parent : transform.parent.parent).localScale.x * Size;
    //        _light.range = transform.parent.localScale.x * Size;
    //
    ////        _isDirty = false;
    //    }

    private void Update()
    {
        if (SceneManager.GetActiveScene().name != "main")
        {
            GameObjectUtil.Destroy(gameObject);
        }
    }
}