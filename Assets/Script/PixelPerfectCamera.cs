﻿using UnityEngine;
using System.Collections;

public class PixelPerfectCamera : MonoBehaviour {

	public static float PixelsToUnits = 1f;
	public static float Scale = 1f;
    public static Camera Camera;
	public Vector2 NativeResolution = new Vector2 (240, 160);

	void Awake () {
		var cam = GetComponent<Camera> ();

	    if (Camera == null)
	        Camera = cam;
		if (cam.orthographic) {
			Scale = Screen.height/NativeResolution.y;
			PixelsToUnits = Scale;
			cam.orthographicSize = (Screen.height / 2.0f) / PixelsToUnits;
		}
	}

}
