using UnityEngine;

public class Ring : MonoBehaviour, IRecycle
{
    public Material[] Materials;
    public Vector2 ScaleRange = new Vector2(7, 9);
    public Vector2 XRotationRange = new Vector2(-20, 20);
    public Vector2 ZRotationRange = new Vector2(0, 180);

    private Transform _parent;
    public Transform Parent
    {
        get { return _parent; }
        set
        {
            _parent = value;

            if (!_renderer) _renderer = GetComponent<Renderer>();
            if (!_renderer) return;

            var scale = Parent.localScale * ScaleRange.Random();
            var rotationX = XRotationRange.Random();
            var rotationZ = ZRotationRange.Random();

            transform.localScale = scale.WithY(1);
            transform.Rotate(rotationX, 0, rotationZ);
            _renderer.material = Materials.RandomElement();
        }
    }

    private Renderer _renderer;

    private void Awake()
    {
        _renderer = GetComponent<Renderer>();
    }
    
    private void Update()
    {
        if (Parent != null)
        {
            transform.position = Parent.position;
        }
    }


    public void Restart()
    {
        if (Parent == null) return;

        print("ring restart");

        var scale = Parent.localScale.x * ScaleRange.Random();
        var rotation = XRotationRange.Random();

        print(new Vector3(scale, 1, scale));
        transform.localScale = new Vector3(scale, 1, scale);
        transform.Rotate(Vector3.right, rotation);
        _renderer.material = Materials.RandomElement();
    }

    public void Shutdown()
    {
    }
}