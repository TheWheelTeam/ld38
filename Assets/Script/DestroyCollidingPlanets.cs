using UnityEngine;

public class DestroyCollidingPlanets : MonoBehaviour
{
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("PullPlanet") || other.gameObject.CompareTag("Sun") || other.gameObject.CompareTag("Sun"))
        {
            GameObjectUtil.Destroy(gameObject);
            print("Destroyed colliding planets!");
        }
    }
}