﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetMove : MonoBehaviour
{
    public float MaxSpeed = 100f;
    public float AccelerationModifier = 10f;
    private Rigidbody _rigidbody;


    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    // Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
	    
	    var vector = new Vector3(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")).normalized;
        if (Mathf.Abs(_rigidbody.velocity.x) > MaxSpeed)
        {
            if ((_rigidbody.velocity.x > 0 && vector.x > 0) || (_rigidbody.velocity.x < 0 && vector.x < 0))
            {
                vector.x = 0;
            }
        }
        if (Mathf.Abs(_rigidbody.velocity.y) > MaxSpeed)
        {
            if ((_rigidbody.velocity.y > 0 && vector.y > 0) || (_rigidbody.velocity.y < 0 && vector.y < 0))
            {
                vector.y = 0;
            }
        }

        _rigidbody.AddForce(vector * AccelerationModifier);
	}
}
