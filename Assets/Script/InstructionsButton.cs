﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstructionsButton : MonoBehaviour
{

    public GameObject InstructionsPanel;
    public void OnClick()
    {
        if(InstructionsPanel == null) return;
        InstructionsPanel.SetActive(!InstructionsPanel.activeSelf);
    }
}
