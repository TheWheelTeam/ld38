﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPool : MonoBehaviour
{

    public RecycleGameObject prefab;
    private List<RecycleGameObject> _poolInstances = new List<RecycleGameObject>();

    void Awake()
    {
        DontDestroyOnLoad(this);
    }
    private RecycleGameObject CreateInstance(Vector3 pos)
    {
        var clone = Instantiate(prefab);
        clone.transform.position = pos;
        clone.transform.SetParent(transform);
        _poolInstances.Add(clone);
        return clone;
    }

    public RecycleGameObject NextObject(Vector3 pos)
    {
        foreach (var inst in _poolInstances)
        {
            if(inst == null || inst.gameObject == null) continue;
            if (!inst.gameObject.activeSelf)
            {
                inst.transform.position = pos;
                inst.Restart();
                return inst;
            }
        }
        //Create a new instance if cannot recycle one
        RecycleGameObject instance = null;
        instance = CreateInstance(pos);
        instance.Restart();
        return instance;
    }
}
