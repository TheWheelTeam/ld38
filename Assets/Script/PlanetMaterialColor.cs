using System;
using System.Collections.Generic;
using UnityEngine;

public class PlanetMaterialColor : MonoBehaviour
{
    public static PlanetMaterialColor Instance { get; private set; }

    public Color Sun;
    public Color World;
    public Color Planet1;
    public Color Planet2;
    public Color Planet3;
    public Color Planet4;
    public Color Planet5;
    public Color Planet6;
    public Color Planet7;
    public Color Planet8;

    private void Awake()
    {
        Instance = this;
    }

    public static Color ColorFromMaterial(string materialName)
    {
        switch (materialName)
        {
            case "Sun Material":
            case "Sun Material (Instance)": return Instance.Sun;

            case "Planet Material":
            case "Planet Material (Instance)": return Instance.World;

            case "Planet Material 2":
            case "Planet Material 2 (Instance)": return Instance.Planet2;

            case "Planet Material 3":
            case "Planet Material 3 (Instance)": return Instance.Planet3;

            case "Planet Material 4":
            case "Planet Material 4 (Instance)": return Instance.Planet4;

            case "Planet Material 5":
            case "Planet Material 5 (Instance)": return Instance.Planet5;

            case "Planet Material 6":
            case "Planet Material 6 (Instance)": return Instance.Planet6;

            case "Planet Material 7":
            case "Planet Material 7 (Instance)": return Instance.Planet7;

            case "Planet Material 8":
            case "Planet Material 8 (Instance)": return Instance.Planet8;

            default:
                print("Unknown material name: " + materialName);
                return Color.black;
        }
    }
}