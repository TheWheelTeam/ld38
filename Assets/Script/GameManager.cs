﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public Vector2 Borders = new Vector2(-1000, 1000);
    private GameObject PlayerGameObject;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
            SceneManager.LoadScene("menu");
        if (PlayerGameObject == null)
            PlayerGameObject = GameObject.FindGameObjectWithTag("Player");
        if (PlayerGameObject != null)
        {
            var pos = PlayerGameObject.transform.position;
            if (pos.x >= Borders.y)
                pos.x = Borders.x + 10;
            if (pos.x <= Borders.x)
                pos.x = Borders.y - 10;
            if (pos.y >= Borders.y)
                pos.y = Borders.x + 10;
            if (pos.y <= Borders.x)
                pos.y = Borders.y - 10;
            PlayerGameObject.transform.position = pos;
            if (PixelPerfectCamera.Camera)
            {
                PixelPerfectCamera.Camera.transform.position = new Vector3(pos.x, pos.y, PixelPerfectCamera.Camera.transform.position.z);
            }
        }
    }
}
