﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffScreenDestroy : MonoBehaviour
{
    public float DestroyOffet = 500f;

    private Transform _playerTransform;
	// Use this for initialization
	void Start ()
	{
	    _playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
	    DestroyOffet = 2 * Screen.width/PixelPerfectCamera.PixelsToUnits;
	}
	
	// Update is called once per frame
	void Update () {
		if(_playerTransform == null) return;

        if(((Vector2)_playerTransform.position - (Vector2)transform.position).magnitude > DestroyOffet)
            GameObjectUtil.Destroy(gameObject);
	}
}
