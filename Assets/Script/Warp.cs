using UnityEngine;

public class Warp : MonoBehaviour
{
    private Vector3 _target;

    private float _minDistanceToWarpSqr = 10000;
    private Rigidbody _rigidbody;
    private bool _warpStarted;
    private float _warpStartTime;
    private readonly bool[] _preWarpConditions = new bool[2];
    private GameObject _mainCamera;
    private readonly Material[] _backgrounds = new Material[3];
    private readonly Vector2[] _offsets = new Vector2[3];
    private bool _backgroundResetRequired;
    private Transform _blackhole;
    private Vector2 _borders;

    public float SqrDistToBlackHole = 45000;
    public float WarpDuration = 2;
    public float SpeedMultiplier = 2;
    public bool ShouldWarp;

    private Transform Blackhole
    {
        get
        {
            if (!_blackhole) _blackhole = GameObject.FindGameObjectWithTag("BlackHole").transform;
            return _blackhole;
        }
    }

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _borders = FindObjectOfType<GameManager>().Borders;
    }
    
    private void Start()
    {
        _mainCamera = GameObject.FindWithTag("MainCamera");
        var renderers = _mainCamera.GetComponentsInChildren<Renderer>();
        for (var i = 0; i < renderers.Length; i++)
        {
            _backgrounds[i] = renderers[i].material;
            _offsets[i] = _backgrounds[i].mainTextureOffset;
        }
    }

    private void Update()
    {
        var difference = Blackhole.position - transform.position;
        if (difference.sqrMagnitude < SqrDistToBlackHole && Input.GetKey(KeyCode.R))
        {
            _target = -difference.normalized * (Random.value * 90000 + 10000);
            TextManager.Instance.HideText();
            ShouldWarp = true;
        }
    }

    private void FixedUpdate()
    {
        if (_backgroundResetRequired)
        {
            ResetBackgrounds();
        }
        if (!ShouldWarp)
        {
            return;
        }

        var accel = _target - transform.position;

        if (_warpStarted && Time.fixedTime - _warpStartTime > WarpDuration)
        {
            _rigidbody.velocity = _rigidbody.velocity.normalized;
            transform.position = transform.position.WithXY(Random.Range(_borders.x, _borders.y),
                Random.Range(_borders.x, _borders.y));

            ShouldWarp = false;
            _warpStarted = false;
            _backgroundResetRequired = true;
            return;
        }
        if (_warpStarted)
        {
            _rigidbody.velocity *= SpeedMultiplier;
            return;
        }

        if (_warpStarted)
        {
            _rigidbody.velocity *= SpeedMultiplier;
            transform.position -= _rigidbody.velocity;
            return;
        }

        if (!_warpStarted)
        {
            _target *= 1000000;
            _warpStarted = true;
            _preWarpConditions[0] = ConditionX();
            _preWarpConditions[1] = ConditionY();
            _warpStartTime = Time.fixedTime;

            _rigidbody.velocity = accel.normalized * 200000;
        }
    }

    private bool ConditionX()
    {
        return _target.x < transform.position.x;
    }

    private bool ConditionY()
    {
        return _target.y < transform.position.y;
    }

    // required after high speed warps that mess up the background materials
    private void ResetBackgrounds()
    {
        for (var i = 0; i < _backgrounds.Length; i++)
        {
            _backgrounds[i].mainTextureOffset = _offsets[i];
        }
        _backgroundResetRequired = false;
    }
}