﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    private Transform _playerTransform;
    private AudioSource _audio;
    // Use this for initialization
    void Awake()
    {
        _audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            _audio.volume = _audio.volume <= float.Epsilon ? .3f : 0;
        }
        if (_playerTransform == null)
        {
            var player = GameObject.FindGameObjectWithTag("Player");
            if (player == null) return;
            _playerTransform = player.transform;

        }
        if (_playerTransform == null) return;
        var velocity = _playerTransform.gameObject.GetComponent<Rigidbody>().velocity;
        transform.position = Vector2.Lerp(transform.position, _playerTransform.position, 10 * Time.deltaTime);
        transform.position = new Vector3(transform.position.x, transform.position.y, -10000);
        transform.position += velocity / 4;
    }
}
