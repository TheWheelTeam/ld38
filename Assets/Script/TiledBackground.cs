﻿using UnityEngine;
using System.Collections;

public class TiledBackground : MonoBehaviour {

	public Vector2 ScaleSize = new Vector2(32,32);
	public bool ScaleHorizontially = true;
	public bool ScaleVertically = true;

	// Use this for initialization
	void Awake () {
	
		var newWidth = !ScaleHorizontially ? 1 : Mathf.Ceil (Screen.width / (ScaleSize.x * PixelPerfectCamera.Scale));
		var newHeight = !ScaleVertically ? 1 : Mathf.Ceil (Screen.height / (ScaleSize.y * PixelPerfectCamera.Scale));

		transform.localScale = new Vector3 (newWidth * ScaleSize.x, newHeight * ScaleSize.y, 1);

		GetComponent<Renderer> ().material.mainTextureScale = new Vector3 (newWidth, newHeight, 1);
	}

}
