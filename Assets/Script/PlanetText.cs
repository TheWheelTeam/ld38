﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetText : MonoBehaviour
{

    public string color = "#FFFFFF";
    public GameObject MapSprite;
	// Use this for initialization
	void Awake () {
		if(MapSprite)
            MapSprite.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public string Text;
    public int Index;
    public AudioClip Audio;
}
