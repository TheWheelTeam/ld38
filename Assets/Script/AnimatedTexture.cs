﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatedTexture : MonoBehaviour,IRecycle
{
    public Vector2 Speed = new Vector2(0.1f, 0.1f);
    private Material _material;
    // Use this for initialization
    void Awake()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(_material == null && GetComponent<MeshRenderer>() != null)
            _material = GetComponent<MeshRenderer>().material;
        if (_material && GetComponent<MeshRenderer>() != null)
            _material.mainTextureOffset += Speed * Time.deltaTime;
    }

    public void Restart()
    {
        _material = null;
    }

    public void Shutdown()
    {

    }
}
