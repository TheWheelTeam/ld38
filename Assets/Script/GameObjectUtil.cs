﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class GameObjectUtil
{
    public static Dictionary<RecycleGameObject, ObjectPool> Pools = new Dictionary<RecycleGameObject, ObjectPool>();

    public static GameObject Instantiate(GameObject prefab, Vector3 position = default(Vector3), Quaternion rotation = default(Quaternion))
    {
        GameObject instance = null;
        var recycleGameObject = prefab.GetComponent<RecycleGameObject>();
        if (recycleGameObject != null)
        {
            var pool = GetObjectPool(recycleGameObject);
            instance = pool.NextObject(position).gameObject;
        }
        else
        {
            instance = Object.Instantiate(prefab, position, rotation);
        }
        return instance;
    }

    public static T Instantiate<T>(T prefab, Vector3 position = default(Vector3),
        Quaternion rotation = default(Quaternion)) where T : MonoBehaviour
    {
        return Instantiate(prefab.gameObject, position, rotation).GetComponent<T>();
    }

    public static void Destroy(GameObject gameObject, float destroyTime)
    {
        var recycleGameObject = gameObject.GetComponent<RecycleGameObject>();
        if (recycleGameObject != null)
        {
            recycleGameObject.Shutdown(destroyTime);
            return;
        }
        Object.Destroy(gameObject,destroyTime);
    }

    public static void Destroy(GameObject gameObject)
    {
        var recycleGameObject = gameObject.GetComponent<RecycleGameObject>();
        if (recycleGameObject != null)
        {
            recycleGameObject.Shutdown();
            return;
        }
        Object.Destroy(gameObject);
    }

    public static void Destroy(params GameObject[] gameObjects)
    {
        foreach (var gameObject in gameObjects)
        {
            Destroy(gameObject);
        }
    }

    public static ObjectPool GetObjectPool(RecycleGameObject reference)
    {
        ObjectPool pool = null;
        if (reference != null && Pools.ContainsKey(reference))
        {
            pool = Pools[reference];
        }
        else if (reference != null)
        {
            var poolContainer = new GameObject(reference.gameObject.name + "ObjectPool");
            pool = poolContainer.AddComponent<ObjectPool>();
            pool.prefab = reference;
            Pools.Add(reference, pool);
        }
        return pool;
    }
}
