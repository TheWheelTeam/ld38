﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TextDisplayer : MonoBehaviour
{
    public Text Text;

    public void SetText(string text)
    {
        Text.text = text;
    }
}
