﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlanetSpawner : MonoBehaviour
{

    public float RingProbability;

    public RecycleGameObject RingPrefab;
    public RecycleGameObject PlanetPrefab;
    public int MaxActivePlanet= 5;
    public ObjectPool PlanetPool;
    public ObjectPool RingPool;
    public float SpawnRange = 9000;
    public float MinimumSpawn = 500;
    // Use this for initialization
    void Start()
    {
        PlanetPool = GameObjectUtil.GetObjectPool(PlanetPrefab);
        for (int i = 0; i < MaxActivePlanet; i++)
        {
            Spawn();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (PlanetPool == null) return;
        bool hasDisabled = false;
        foreach (Transform child in PlanetPool.transform)
        {
            if (child.gameObject.activeSelf) continue;
            hasDisabled = true;
        }
        if (!hasDisabled && PlanetPool.transform.childCount >= MaxActivePlanet) return;
        Spawn();
    }

    public void Spawn()
    {
        float x;
        do
        {
            x = Random.Range(-SpawnRange, SpawnRange);
        } while (Mathf.Abs(x) < MinimumSpawn);
        float y;
        do
        {
            y = Random.Range(-SpawnRange, SpawnRange);
        } while (Mathf.Abs(y) < MinimumSpawn);
        var instance = GameObjectUtil.Instantiate(PlanetPrefab, new Vector3(x, y, 0) + transform.position);

        if (RingProbability.Roll())
        {
            var ring = GameObjectUtil.Instantiate(RingPrefab, new Vector3(x, y, 0) + transform.position)
                .GetComponent<Ring>();

            ring.Parent = instance.transform;
        }
    }
}
