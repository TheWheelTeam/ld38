﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapCameraController : MonoBehaviour
{

    private Camera _camera;
    // Use this for initialization
    void Awake()
    {
        _camera = GetComponent<Camera>();
        _camera.depth = -2;
    }

    // Update is called once per frame
    void Update()
    {
        if (_camera == null) return;
        if (Input.GetKeyDown(KeyCode.Tab))
            _camera.depth = -_camera.depth;
    }
}
