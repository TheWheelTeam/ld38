﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlText : MonoBehaviour
{
    private PlanetText _currentText;
    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Communicator"))
        {
            _currentText = other.GetComponent<PlanetText>();
            if (_currentText)
            {
                TextManager.Instance.DisplayText(other.GetComponent<PlanetText>());
                _currentText.MapSprite.SetActive(true);
            }
        }
    }

    public void OnTriggerExit(Collider other)
    {
        //if (other.CompareTag("Communicator"))
        //{
        //    if (_currentText.Text == other.GetComponent<PlanetText>().Text)
        //    {
        //        TextManager.Instance.HideText();
        //    }
        //}
    }
}
