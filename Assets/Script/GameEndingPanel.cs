﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameEndingPanel : MonoBehaviour
{
    private Image _endingImage;
    // Use this for initialization
    void Awake()
    {
        _endingImage = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        _endingImage.color = new Color(_endingImage.color.r, _endingImage.color.g, _endingImage.color.b, _endingImage.color.a + 5 * Time.deltaTime);
    }
}
