﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DecorationSpawner : MonoBehaviour
{

    public RecycleGameObject DecorationPrefab;
    public int MaxActiveDecoration = 5;
    public ObjectPool DecorationPool;
    private float r;
    // Use this for initialization
    void Start()
    {
        r = (float)Screen.width / PixelPerfectCamera.PixelsToUnits;
        DecorationPool = GameObjectUtil.GetObjectPool(DecorationPrefab);
        for (int i = 0; i < MaxActiveDecoration; i++)
        {
            Spawn();
        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void Spawn()
    {
        if (DecorationPool == null) return;
        bool hasDisabled = false;
        foreach (Transform child in transform)
        {
            if (child.gameObject.activeSelf) continue;
            hasDisabled = true;

        }
        if(!hasDisabled && transform.childCount > MaxActiveDecoration) return;
        var x = Random.Range(-r, r);
        var ySqr = (r * r) - (x * x);
        var y = Mathf.Sign(Random.Range(-1, 1)) * Mathf.Sqrt(ySqr);
        var instance = GameObjectUtil.Instantiate(DecorationPrefab, new Vector3(x, y, 0) + transform.position);
        instance.transform.SetParent(transform);
    }
}
