﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MonoBehaviour,IRecycle
{

    public Material[] Materials;
    public Vector2 PlanetScaleRange = new Vector2(50,200);
    private MeshRenderer _meshRenderer;

    void Awake()
    {
        _meshRenderer = GetComponent<MeshRenderer>();
    }

    void Update()
    {
        if(Mathf.Abs(transform.position.x) > 10000 || Mathf.Abs(transform.position.y) > 10000)
            GameObjectUtil.Destroy(transform.parent.gameObject);
    }
    public void Restart()
    {
        if(!_meshRenderer) return;
        var scale = Random.Range(PlanetScaleRange.x, PlanetScaleRange.y);
        transform.parent.localScale = new Vector3(scale,scale, scale);
        _meshRenderer.material = Materials.RandomElement();
    }


    public void Shutdown()
    {
       
    }
}
