﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlidingBackground : MonoBehaviour
{

    public float SlideSpeedModifier;
    private Rigidbody _body;

    private Material _material;

    // Use this for initialization
    void Awake()
    {
        _material = GetComponent<MeshRenderer>().material;
        var player = GameObject.FindGameObjectWithTag("Player");
        if (player != null)
            _body = player.GetComponent<Rigidbody>();

    }

    void Start()
    {
        GetComponent<MeshRenderer>().sortingOrder = (int)-(SlideSpeedModifier * 1000);
    }

    // Update is called once per frame
    void Update()
    {
        if (_body == null) return;
        var scale = transform.localScale;
        var velocity = _body.velocity * SlideSpeedModifier * Time.deltaTime;
        _material.mainTextureOffset += (Vector2)velocity;
    }
}
