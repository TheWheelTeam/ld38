﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface IRecycle
{
    void Restart();
    void Shutdown();
}
public class RecycleGameObject : MonoBehaviour
{
    public List<IRecycle> RecycleComponents;

    void Awake()
    {
        var components = GetComponents<MonoBehaviour>();
        RecycleComponents = new List<IRecycle>();
        foreach (var monoBehaviour in components)
        {
            if (monoBehaviour is IRecycle)
            {
                RecycleComponents.Add(monoBehaviour as IRecycle);
            }
        }

    }
    public void Restart()
    {
        gameObject.SetActive(true);
        foreach (var component in RecycleComponents)
        {
            component.Restart();
        }
        foreach (var component in transform.GetComponentsInChildren<IRecycle>())
        {
            component.Restart();
        }
    }

    public void Shutdown(bool setActive = false)
    {
        gameObject.SetActive(setActive);

        foreach (var component in RecycleComponents)
        {
            component.Shutdown();
        }
        foreach (var component in transform.GetComponentsInChildren<IRecycle>())
        {
            component.Shutdown();
        }
    }
    public void Shutdown(float destroyTime, bool setActive = false)
    {
        StartCoroutine(ShutdownDelayRoutine(destroyTime, setActive));
    }

    private IEnumerator ShutdownDelayRoutine(float destroyTime, bool setActive)
    {
        yield return new WaitForSeconds(destroyTime);
        Shutdown(setActive);
    }
}
