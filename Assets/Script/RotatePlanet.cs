using System;
using UnityEngine;

public class RotatePlanet : MonoBehaviour
{
    public float Angle;


    private void Awake()
    {
    }

    private void Update()
    {
        transform.Rotate(Vector3.up, Angle);
    }
}