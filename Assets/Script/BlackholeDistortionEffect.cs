﻿using UnityEngine;
using UnityStandardAssets.ImageEffects;

[ExecuteInEditMode]
public class BlackholeDistortionEffect : PostEffectsBase
{
    public Shader Shader;
    [Range(0, 10000)]
    public float MaxDistance; // distance between player and black hole for distortion

    [Range(-1, 1)]
    public float EffectStrength; // distortion strength
    [Range(0.1f, 10)]
    public float EffectRange; // fractional distance to determine which bits of screen is affected
    [Range(0.2f, 8)]
    public float ZoomLevel; // 1 is default (no zoom) 0.2, 0.5, etc. zoom in, 2, 4, etc. zoom out

    [Range(0, 10000)]
    public float WaveRange; // distance between player and black hole for "wave"
    [Range(0, 128)]
    public float WaveFrequency;
    public Vector4 Dividers; // (frequency.x, frequency.y, magnitude.x, magnitude.y)

    private Material _material;
    private Transform _player;
    private Transform _blackhole;

    private float _distance;
    private bool _isEffectApplicaple;

    public bool IsEffectApplicaple
    {
        get { return _isEffectApplicaple; }
        set
        {
            // true -> false
            if (_isEffectApplicaple && !value)
            {
                _material.SetFloat(MatProps.DistToBlackhole, -1);
                _material.SetVector(MatProps.EffectCenterOffset, Vector2.zero);
            }
            _isEffectApplicaple = value;
        }
    }

    private static class MatProps
    {
        internal const string DistToBlackhole = "_DistToBlackhole";
        internal const string EffectCenterOffset = "_EffectCenterOffset";
        internal const string EffectStrength = "_EffectStrength";
        internal const string EffectRange = "_EffectRange";
        internal const string MaxDistance = "_MaxDistance";
        internal const string ZoomLevel = "_ZoomLevel";
        internal const string Dividers = "_Dividers"; // (frequency.x, frequency.y, magnitude.x, magnitude.y)
        internal const string WaveRange = "_WaveRange";
        internal const string WaveFrequency = "_WaveFrequency";
    }

    private bool InitTransforms()
    {
        if (!_blackhole)
        {
            var blackholeGameObject = GameObject.FindWithTag("BlackHole");
            if (!blackholeGameObject) return false;

            _blackhole = blackholeGameObject.transform;
            if (!_blackhole) return false;
        }

        if (!_player)
        {
            var playerGameObject = GameObject.FindWithTag("Player");
            if (!playerGameObject) return false;

            _player = playerGameObject.transform;
            if (!_player) return false;
        }

        return true;
    }

    public override bool CheckResources()
    {
        _player = GameObject.FindWithTag("Player").transform;
        _material = CheckShaderAndCreateMaterial(Shader, _material);

        return true;
    }

    private new void Start()
    {
        base.Start();

        _material.SetVector(MatProps.Dividers, Dividers);
        _material.SetFloat(MatProps.WaveFrequency, WaveFrequency);
        _material.SetFloat(MatProps.WaveRange, WaveRange);
        _material.SetFloat(MatProps.EffectStrength, EffectStrength);
        _material.SetFloat(MatProps.EffectRange, EffectRange);
        _material.SetFloat(MatProps.MaxDistance, MaxDistance);
        _material.SetFloat(MatProps.ZoomLevel, ZoomLevel);
    }

    private void OnRenderImage(RenderTexture src, RenderTexture dest)
    {
        if (IsEffectApplicaple)
        {
            Graphics.Blit(src, dest, _material);
        }
        else
        {
            Graphics.Blit(src, dest);
        }
    }

    private void FixedUpdate()
    {
        if (!InitTransforms()) return;

        var difference = _player.position - _blackhole.position;
        _distance = difference.magnitude;
        IsEffectApplicaple = _distance.IsInRange(0, Mathf.Max(WaveRange, MaxDistance));

        if (!IsEffectApplicaple) return;

        _material.SetFloat(MatProps.DistToBlackhole, _distance);
        _material.SetVector(MatProps.EffectCenterOffset, -difference.XY().normalized);
    }
}
