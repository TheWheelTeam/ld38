﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateSprite : MonoBehaviour {

    public float Angle;


    private void Awake()
    {
    }

    private void Update()
    {
        transform.Rotate(Vector3.forward, Angle*Time.deltaTime);
    }
}
