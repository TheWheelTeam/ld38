﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Decoration : MonoBehaviour, IRecycle
{

    public Sprite[] Sprites;
    public Vector2 ColliderOffset = new Vector2();

    private DecorationSpawner _spawner;

    public void Restart()
    {
        var spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = Sprites[Random.Range(0, Sprites.Length)];
        var scale = new Vector3((Random.Range(-1, 1) < 0 ? -1 : 1) * transform.localScale.x,
            transform.localScale.y,
            transform.localScale.z);
        transform.localScale = scale;
        transform.rotation = Quaternion.AngleAxis(Random.Range(0,360),Vector3.forward);
    }

    public void Shutdown()
    {
        if (!_spawner)
            _spawner = GameObject.FindGameObjectWithTag("DecorationSpawner").GetComponent<DecorationSpawner>();
        if (_spawner)
            _spawner.Spawn();
    }
}
