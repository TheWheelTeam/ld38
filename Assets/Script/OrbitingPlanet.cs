﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitingPlanet : MonoBehaviour, IRecycle
{


    public RotateNearbyPlanets Orbit;
    public float RotateSpeed = 1;
    void Update()
    {
        if (Orbit == null) return;
        var nextPosition = transform.position.XY().RotateAround(Orbit.transform.position, RotateSpeed * Time.deltaTime);
        transform.position = nextPosition;
    }
    public void Restart()
    {
        Orbit = null;
    }

    public void Shutdown()
    {
    }
}
