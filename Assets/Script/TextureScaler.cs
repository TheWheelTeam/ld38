﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureScaler : MonoBehaviour
{

    public Vector2 textureSize = new Vector2(32, 32);
    public bool scaleHorizontially = true;
    public bool scaleVertically = true;

    // Use this for initialization
    void Awake()
    {
        Transform trans = transform;
        while (trans.parent != null)
            trans = trans.parent;
        if (trans != null)
        {
            var newWidth = !scaleHorizontially ? 1 : trans.localScale.x * transform.localScale.x;
            newWidth /= textureSize.x;
            var newHeight = !scaleVertically ? 1 : trans.localScale.y * transform.localScale.y;
            newHeight /= textureSize.y;
            GetComponent<Renderer>().material.mainTextureScale = new Vector3(newWidth, newHeight, 1);
        }
    }
}
