﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Runtime.Remoting.Messaging;
using System.Text;
using UnityEngine;

public class TextManager : MonoBehaviour
{
    public static TextManager Instance;
    public AudioClip[] AudioClips;
    public TextAsset _textAsset;

    private TextDisplayer _displayer;
    private List<string> _textList;
    private string[] _textArray;
    private AudioSource _audioSource;
    private bool _quit;
    // Use this for initialization
    void Start()
    {
        _quit = true;
        if (Instance == null)
            Instance = this;
        if (_textAsset == null)
            _textAsset = Resources.Load("text") as TextAsset;
        if (_textAsset != null)
            _textList = _textAsset.text.Split('\n').ToList();
        _textArray = _textList.ToArray();
    }

    // Update is called once per frame
    void Update()
    {
        if (_displayer == null)
        {
            var go = GameObject.FindGameObjectWithTag("TextPanel");
            if (go == null) return;
            _displayer = go.GetComponent<TextDisplayer>();
            if (_displayer == null) return;
            _displayer.gameObject.SetActive(false);
        }
        if (_audioSource == null)
        {
            var Cam = GameObject.FindGameObjectWithTag("MainCamera");
            if(Cam == null) return;
            _audioSource = Cam.GetComponent<AudioSource>();
        }
        if (_quit && _audioSource != null)
            _audioSource.Stop();
    }

    public void DisplayText(PlanetText planetText)
    {
        if (_displayer == null) return;
        if(!_quit) return;
        if (string.IsNullOrEmpty(planetText.Text))
        {
            var txt = _textList.Count > 0 ? _textList.RandomElement() : string.Empty;
            planetText.Text = txt;
            planetText.Index = _textArray.ToList().IndexOf(txt);
            if (planetText.Index < AudioClips.Length)
                planetText.Audio = AudioClips[planetText.Index];
            _textList.Remove(txt);
        }
        _quit = false;
        _displayer.gameObject.SetActive(true);
        StartCoroutine(IterateText(planetText));
        PlayMorse(planetText);
    }

    private void PlayMorse(PlanetText planetText)
    {
        if(_audioSource == null) return;
        if(planetText.Index >= _textArray.Length) return;
        if(planetText.Audio == null) return;
        _audioSource.PlayOneShot(planetText.Audio);
    }


    private IEnumerator IterateText(PlanetText planetText)
    {
        var currentText = new StringBuilder();
        for (int i = 0; i < planetText.Text.Length; i++)
        {
            if (_quit)
            {
                currentText = null;
                break;
            }
            currentText.Append(planetText.Text.ToCharArray()[i]);
            _displayer.SetText(string.Format("<color='{1}'>{0}</color>",currentText.ToString(),planetText.color));
            yield return new WaitForSeconds(0.05f);
        }
        if (_audioSource != null)
            _audioSource.Stop();
        yield return new WaitForSeconds(2f);
        HideText();
    }

    public void HideText()
    {
        _quit = true;
        _displayer.gameObject.SetActive(false);
    }
}
