using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class MusicPlayer : MonoBehaviour
{
    private const string DefaultPath = "Music";

    public AudioClip DefaultClip;
    public float RequiredProgressToStream = 1f;

    private readonly AudioClip[] _audioClips = new AudioClip[2];
    private IEnumerator<AudioClip> _clipEnumerator;
    private IEnumerable<AudioClip> _clipEnumerable;
    private AudioSource _music;
    private int _clipIndex;

    private readonly string[] _extensions = { "wav", "ogg" };

    private void Awake()
    {
        _music = GetComponent<AudioSource>();
    }

    private void Start()
    {
        var path = PlayerPrefs.GetString("music_path");
        if (string.IsNullOrEmpty(path))
        {
            print("Music path not set. Searching the default directory...");
            path = DefaultPath;
        }

        var directory = new DirectoryInfo(path);

        _clipEnumerable = AudioClips(directory);
        ResetClipEnumerator();
        _audioClips[0] = _clipEnumerator.Current;
        if (_clipEnumerator.MoveNext())
        {
            _audioClips[1] = _clipEnumerator.Current;
        }

        if (!_audioClips[0])
        {
            print("No clips to play. Playing default clip...");
            _music.loop = true;
            _music.clip = DefaultClip;
            _music.Play();
            return;
        }

        if (_audioClips[0] && !_audioClips[1])
        {
            print("One clip loaded. Looping enabled.");
            _music.loop = true;
            _music.clip = _audioClips[0];
            _music.Play();
            return;
        }

        print("Multiple clips loaded.");
        _music.loop = false;
    }

    private void ResetClipEnumerator()
    {
        _clipEnumerator = _clipEnumerable.GetEnumerator();
        _clipEnumerator.MoveNext();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            _music.volume = _music.volume <= float.Epsilon ? 1 : 0;
        }
        if (_music.isPlaying || _music.loop) return;
        if (_music.clip != null)
        {
            print(_music.timeSamples + " / " +_music.clip.samples);
            if (_music.clip.samples != _music.timeSamples) return;
        }

        _music.clip = _audioClips[_clipIndex];
        _music.Play();

        // if no more songs left, replay "playlist"
        if (!_clipEnumerator.MoveNext())
        {
            ResetClipEnumerator();
        }

        _clipIndex = (_clipIndex + 1) % 2;
        Destroy(_audioClips[_clipIndex]);
        _audioClips[_clipIndex] = _clipEnumerator.Current;
    }

    private IEnumerable<AudioClip> AudioClips(DirectoryInfo directory)
    {
        while (!directory.Exists) yield return null;

        var files = directory.GetFiles();
        for (var i = 0; i < files.Length; i++)
        {
            var file = files[i];
            if (IsFileValid(file))
            {
                yield return Load(file);
            }
        }
    }

    private bool IsFileValid(FileSystemInfo file)
    {
        for (var i = 0; i < _extensions.Length; i++)
        {
            var ext = _extensions[i];
            if (file.Name.EndsWith(ext))
            {
                return true;
            }
        }
        return false;
    }

    private AudioClip Load(FileSystemInfo file)
    {
        // Error: Cannot create FMOD::Sound instance for resource (rB, (Operation could not be performed because specified sound/DSP connection is not ready. )
        // prints ^ but works?

        var clip = new WWW("file://" + file.FullName).GetAudioClip(false, true);

        if (!clip.LoadAudioData())
        {
            print("Failed to load audio. " + file.Name);
        }

        while (clip.loadState == AudioDataLoadState.Unloaded || clip.loadState == AudioDataLoadState.Loading) { }
        print(clip.loadState);

        return clip;
    }
}