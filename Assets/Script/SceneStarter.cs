﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SceneObject
{
    public GameObject Prefab;
    public Vector3 Position;
}
public class SceneStarter : MonoBehaviour {


    public SceneObject[] SceneObjects;
    //public GameObject[] SceneObjects;
    void Start()
    {
        SetTheScene();
    }

    private void SetTheScene()
    {
        foreach (var sceneObject in SceneObjects)
        {
            var instance = GameObjectUtil.Instantiate(sceneObject.Prefab, sceneObject.Position);
        }

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
