using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ForcePull : MonoBehaviour
{
    public float NormalForce = 1;
    public RecycleGameObject WavePrefab;
    public int WaveSpeedMultiplier = 12;

    private Rigidbody _rigidbody;
    private SphereCollider _sphereCollider;
    private List<Rigidbody> _bodiesInRange;


    private const float G = (float)6.67408E-11;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _sphereCollider = GetComponent<SphereCollider>();
        _bodiesInRange = new List<Rigidbody>();
    }
    private void FixedUpdate()
    {
        if (SceneManager.GetActiveScene().name != "main")
        {
            _bodiesInRange = new List<Rigidbody>();
            GameObjectUtil.Destroy(gameObject);
            return;
        }
        foreach (var body in _bodiesInRange)
        {
            var direction = (Vector2)body.transform.position - (Vector2)transform.position;
            var distance = direction.magnitude;
            var force = body.mass * _rigidbody.mass / (distance * distance) * NormalForce;

            var forceVector = force * direction;
            if (((Vector3)forceVector).HasNanComponent())
            {
                break;
            }

            _rigidbody.AddForce(forceVector);
            body.AddForce(-forceVector, ForceMode.Force);

//            Debug.DrawLine(transform.position, body.transform.position, Color.yellow);
            Debug.DrawRay(transform.position, direction, Color.yellow);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
//        other.gameObject.layer = LayerMask.NameToLayer("");
        var body = other.GetComponent<Rigidbody>();
        if (body != null && body.CompareTag("Player"))
        {
            _bodiesInRange.Add(body);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        _bodiesInRange.Remove(other.GetComponent<Rigidbody>());
    }
}