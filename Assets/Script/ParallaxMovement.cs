﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxMovement : MonoBehaviour, IRecycle
{
    public float MovementFactor = -0.6f;
    private Rigidbody _relativeMovementObject;
    private SpriteRenderer _spriteRenderer;
    private float _currentMovementFactor;
    // Use this for initialization
    void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_relativeMovementObject == null) return;
        transform.position += _relativeMovementObject.velocity * Time.deltaTime * _currentMovementFactor;
    }

    public void Restart()
    {
        _currentMovementFactor = MovementFactor + Random.Range(0, 0.05f);
        _relativeMovementObject = GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody>();
        _spriteRenderer.sortingOrder = -(int)(_currentMovementFactor * 1000);
    }

    public void Shutdown()
    {
    }
}
