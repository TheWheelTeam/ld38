using System.Collections.Generic;
using UnityEngine;

public class RotateNearbyPlanets : MonoBehaviour
{
    [SerializeField]
    private List<Transform> _orbitingPlanets;

    private void Awake()
    {
        _orbitingPlanets = new List<Transform>();
    }

    private void OnTriggerEnter(Collider other)
    {
        var script = other.GetComponent<OrbitingPlanet>();
        if (other.CompareTag("PullPlanet") && script != null)
        {
            _orbitingPlanets.Add(other.transform);
            script.Orbit = this;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        var script = other.GetComponent<OrbitingPlanet>();
        if (other.CompareTag("PullPlanet") && script != null && script.Orbit == this)
        {
            script.Orbit = null;
        }
    }

}