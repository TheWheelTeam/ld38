﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;
using Rng = UnityEngine.Random;

public static class Extensions
{
    public static float Random(this Vector2 v) { return Rng.Range(v.x, v.y); }

    public static bool Roll(this float f) { return Rng.value < f; }

    public static float SqrDistanceTo(this Vector2 v, Vector2 w) { return (v - w).sqrMagnitude; }
    public static float DistanceTo(this Vector2 v, Vector2 w) { return (v - w).magnitude; }

    public static float SqrDistanceTo(this Vector3 v, Vector3 w) { return (v - w).sqrMagnitude; }
    public static float DistanceTo(this Vector3 v, Vector3 w) { return (v - w).magnitude; }

    public static Vector2 WithX(this Vector2 v, float x) { return new Vector2(x, v.y); }
    public static Vector2 WithY(this Vector2 v, float y) { return new Vector2(v.x, y); }

    public static Vector2 AddX(this Vector2 v, float x) { return new Vector2(v.x + x, v.y); }
    public static Vector2 AddY(this Vector2 v, float y) { return new Vector2(v.x, v.y + y); }

    public static Vector3 WithX(this Vector3 v, float x) { return new Vector3(x, v.y, v.z); }
    public static Vector3 WithY(this Vector3 v, float y) { return new Vector3(v.x, y, v.z); }
    public static Vector3 WithZ(this Vector3 v, float z) { return new Vector3(v.x, v.y, z); }

    public static Vector3 WithXY(this Vector3 v, float x, float y) { return new Vector3(x, y, v.z); }

    public static Vector3 AddX(this Vector3 v, float x) { return new Vector3(v.x + x, v.y, v.z); }
    public static Vector3 AddY(this Vector3 v, float y) { return new Vector3(v.x, v.y + y, v.z); }
    public static Vector3 AddZ(this Vector3 v, float z) { return new Vector3(v.x, v.y, v.z + z); }

    public static Vector2 XY(this Vector3 v, Vector2 offset) { return new Vector2(v.x + offset.x, v.y + offset.y); }
    public static Vector2 XY(this Vector3 v, float x = 0, float y = 0) { return new Vector2(v.x + x, v.y + y); }
    public static Vector2 XZ(this Vector3 v, float x = 0, float z = 0) { return new Vector2(v.x + x, v.z + z); }
    public static Vector2 YZ(this Vector3 v, float y = 0, float z = 0) { return new Vector2(v.y + y, v.z + z); }

    public static Vector2 RotateAround(this Vector2 point, Vector2 pivot, float zAngle) {
        return (Quaternion.Euler(0, 0, zAngle) * (point - pivot)).XY() + pivot;
    }

    public static Vector2 RotateAround(this Vector2 point, Vector2 pivot, Vector3 angles) {
        return (Quaternion.Euler(angles) * (point - pivot)).XY() + pivot;
    }

    public static bool ApproxXY(this Vector3 v, Vector2 t) { return v.x.Approx(t.x) && v.y.Approx(t.y); }
    public static bool Approx(this Vector2 v, Vector2 t) { return v.x.Approx(t.x) && v.y.Approx(t.y); }

    public static bool Approx(this float f, float other) { return Mathf.Abs(f - other) < float.Epsilon; }

    public static bool Approx(this Quaternion q, Quaternion other)
    {
        return Quaternion.Angle(q, other) < Quaternion.kEpsilon;
    }

    public static bool IsInRange(this float i, float min, float max) { return i > min && i < max; }

    public static T RandomElement<T>([NotNull] this IEnumerable<T> enumerable)
    {
        var array = enumerable as T[] ?? enumerable.ToArray();

        if (array.Length == 0)
        {
            throw new Exception("Collection must not be empty.");
        }

        return array.ElementAt(Rng.Range(0, array.Length));
    }

    public static bool HasNanComponent(this Vector3 self)
    {
        if (float.IsNaN(self.x)) return true;
        if (float.IsNaN(self.y)) return true;
        if (float.IsNaN(self.z)) return true;
        return false;
    }
}
